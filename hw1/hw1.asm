.data  
#example input  
values:  .word   55, 25, 3, 41, 61, 200, 14, 8, 2 , 25, 35 #array contents
size:    .word 11 

first:    .word 13254689527
second:   .word 13254689527
maxValue: .word 13254689527
massage: .asciiz "No second smallest"



.text


lw $s0 size
lw $s1 first
lw $s2 second
lw $s3 maxValue
add $t2 $zero 0x10010000

blt $s0 2 exit

addi $t0 $zero 0  # set i to 0
loop: bge $t0 $s0 exit_loop  #if i  > size
addi $t0 $t0 1               # i + 1
lw $t5 ($t2)
addi $t2 $t2 4
bge $t5 $s1 else
move $s2 $s1                # second = first
move $s1 $t5                # first = arr[i]
else:
bge $t5 $s2 loop
beq $t5 $s1 loop
move $s2 $t5
j loop
exit_loop:

beq $s2 $s3 exit            # if (second == Integer.MAX_VALUE)
li $v0 1
move $a0 $s2
syscall

add $v0 $zero 10
syscall


#if you find the second smallest print it 
#if all the numbers in the array are the same print "No second smallest"
exit:
 li $v0 4
 la $a0 massage
 syscall

